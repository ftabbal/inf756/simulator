module Behaviours.Move.Random where

import Data.List ((\\))
import Control.Monad.Random
import System.Random.Shuffle
import System.Random
import System.Random.Stateful (uniformRM, uniformM)

type Node = Int

move :: (MonadRandom m) => Node -> [Node] -> Node -> m Node
-- no neighbours from node, should not happen
-- move from neighbours = case neighbours of
move from [] previous = pure previous
-- only one neighbour node, should be previous
move from (x:[]) previous = pure x
-- corridor, one of the neighbours is the previous node
-- select the other one
move from (x:y:[]) previous = if x == previous
                              then pure y
                              else pure x
move from neighbours previous = head <$> (shuffleM (neighbours \\ [previous]))