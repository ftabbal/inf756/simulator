module Terrain (Maze, newMaze, neighbours, cells, toString) where

import Data.Graph.Inductive (prettify)

import qualified Data.Graph.Inductive as Graph
import qualified Data.Text as T
import qualified Data.Either as E

import Data.Text.Read
import Data.Graph.Inductive (Gr)

type Maze = Gr () ()

parseEntry :: T.Text -> (Int, [Int])
parseEntry a = (h, neighb)
  where h = head nodeList
        neighb = tail nodeList
        res dec = E.fromRight (-1, "NA") (decimal dec)
        nodeList = fmap fst $ [res x | x <- (T.splitOn ";" a)]

newMaze :: [T.Text] -> Maze
newMaze adjacencyList = Graph.mkGraph nodes edges
  where nodeMap = fmap parseEntry adjacencyList
        nodes = [(x, ()) | x <- (fmap fst nodeMap)]
        edgeList entry = [(fst entry, n, ()) | n <- (snd entry)]
        edges = concat $ fmap edgeList nodeMap

neighbours :: Maze -> Int -> [Int]
neighbours graph node = Graph.suc graph node

cells :: Maze -> [Int]
cells maze = Graph.nodes maze

toString :: Maze -> T.Text
toString graph = T.pack $ prettify graph