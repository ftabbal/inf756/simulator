module Types.Food where

data Food = Food
    { cellId :: Int
    , healthRestoreValue :: Int
    } deriving (Show)
