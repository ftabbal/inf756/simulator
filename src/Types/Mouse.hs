module Types.Mouse where

data Mouse = Mouse
    { identifier :: Int
    , currentPos :: Int
    , previousPos :: Int
    , health :: Int
    , psy :: Int
    , speedFactor :: Double
    } deriving (Show)


-- updateHealth :: Mouse -> Int -> Mouse
-- updateHealth mouse val = mouse { health = health + val}

-- updatePsy :: Mouse -> Int -> Mouse
-- updatePsy mouse val = mouse { health = health + val}


-- update :: Mouse -> Int -> Mouse
-- update mouse newPos = mouse { currentPos = newPos previousPos = (currentPost mouse)}
