{-# LANGUAGE ScopedTypeVariables #-}
module Main (main) where

import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString.Lazy.Char8 as BC
import qualified Network.WebSockets as WS
import Control.Concurrent
import Control.Concurrent.Async
import Control.Concurrent.STM
import qualified Data.Map as Map
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Random
import Data.Aeson

import Terrain
import Behaviours.Move.Random
import Types.Mouse
import Types.Food

-- 1. Load Maze
-- 2. Load rules
-- 3. Set up mice
-- 4. Set food parameters
-- 5. Run

-- Run
-- Each turn:
--   handle mouse
--   check for termination condition
--   Send to client

-- Each mouse do
--   Send environment to process
--   On return -> apply action to environment
--     Conflict -> retry
-- 

-- Config:
--   - Maze
--   - Rules
--      - Termination
--      - Running
--   - Mice
--      - Behaviour
--      - Move
--      - Starting Position
--   Food
--      - Position
--      - Apparition rule



instance FromJSON Mouse where
    parseJSON (Object m) =
        Mouse <$> m .: "id"
              <*> m .: "currentCell"
              <*> m .: "previousCell"
              <*> m .: "health"
              <*> m .: "psy"
              <*> m .: "speedFactor"

instance FromJSON Food where
    parseJSON (Object f) = 
        Food <$> f .: "cellId"
             <*> f .: "healthRestoreValue"

data Config = Config
    { mazeData :: [T.Text]
    , mouseList :: [Mouse]
    , foodList :: [Food]
    } deriving (Show)

instance FromJSON Config where
    parseJSON (Object c) = 
        Config <$> c .: "adjacencyList"
               <*> c .: "mice"
               <*> c .: "foodList"

data FoodLocations = FoodLocations
    { foodMap :: TVar (Map.Map Cell HealthRestoreValue)}

type Cell = Int
type HealthRestoreValue = Int

-- Connection to the simulator client
type Client = WS.Connection



mv :: (MonadRandom m) => Maze -> Cell -> Cell -> m (Cell, Cell)
mv maze from previous = do
                    let neigh = neighbours maze from
                    target <- move from neigh previous
                    pure (target, from)

moveMouse :: Client -> Maze -> FoodLocations -> Mouse -> IO ()
moveMouse client maze foodLocations mouse = do
                            (newCurr, newPrev) <- mv maze currPos prevPos
                            foodVal <- takeFood foodLocations newCurr
                            let updatedMouse = mouse { currentPos = newCurr
                                                     , previousPos = newPrev
                                                     , health = mouseHealth + (hpChange foodVal)
                                                     , psy = mousePsy + (psyChange foodVal)                                                     }

                            TIO.putStrLn $ mouseInfo updatedMouse

                            WS.sendTextData client $ mouseInfo updatedMouse
                            if (mouseHealth > 0 && mousePsy > 0)
                            then
                                -- TODO to make cleaner
                                -- Mouse ate
                                if (health mouse < health updatedMouse)
                                then do
                                    TIO.putStrLn $ mouseAteAt newCurr
                                    WS.sendTextData client $ mouseAteAt newCurr
                                    threadDelay $ round $ 500000 * (speedFactor updatedMouse)
                                    moveMouse client maze foodLocations updatedMouse
                                else do    
                                    threadDelay $ round $ 500000 * (speedFactor updatedMouse)
                                    moveMouse client maze foodLocations updatedMouse
                            else do
                                TIO.putStrLn deadMouse
                                WS.sendTextData client deadMouse

  where mouseInfo m = T.pack $ (show (identifier m)) ++ ";" ++
                               (show (currentPos m)) ++ ";" ++
                               (show (health m))     ++ ";" ++
                               (show (psy m))
        mouseId = identifier mouse
        currPos = currentPos mouse
        prevPos = previousPos mouse
        deadMouse = T.pack $ (show mouseId) ++ ";" ++ "dead"
        mouseAteAt cellId = T.pack $ (show mouseId) ++ ";" ++ "ate" ++ ";" ++ (show cellId)
        mouseHealth = health mouse
        mousePsy = psy mouse
        hpChange foodVal = case foodVal of
                                    Just val -> val
                                    Nothing -> (-1)
        psyChange foodVal = case foodVal of
                                    Just _ -> 3
                                    Nothing -> (-1)
        -- delay = 

newFoodLocations :: IO FoodLocations
newFoodLocations = do
                f <- newTVarIO Map.empty
                return FoodLocations { foodMap = f}

makeFoodLocations :: [Food] -> IO FoodLocations
makeFoodLocations foodList = do
                   fl <- newFoodLocations
                   forM_ foodList (\x -> addFood fl x)
                   pure fl

addFood :: FoodLocations -> Food -> IO ()
addFood foodLocation foodItem = atomically $ do 
                        food <- readTVar  $ foodMap foodLocation
                        writeTVar (foodMap foodLocation) $ Map.insert (cellId foodItem) (healthRestoreValue foodItem) food
                        pure ()

takeFood :: FoodLocations -> Cell -> IO (Maybe HealthRestoreValue)
takeFood foodLocation cellId = atomically $ do
                food <- readTVar $ foodMap foodLocation
                if Map.notMember cellId food
                then return Nothing
                else do
                    let val = Map.lookup cellId food
                    writeTVar (foodMap foodLocation) $ Map.delete cellId food
                    return val

loadConfig :: WS.Connection -> IO (Either String Config)
loadConfig conn = do
    msg <- WS.receiveData conn :: IO BC.ByteString
    BC.putStrLn msg
    let eitherData = eitherDecode msg :: Either String Config
    case eitherData of
        Left err -> do
            pure $ Left err
        Right confData -> pure $ Right confData


startMouseService :: Client -> Maze -> FoodLocations -> Mouse -> IO ()
startMouseService client maze foodLocations mouse = do
                WS.sendTextData client notif
                TIO.putStrLn notif
                moveMouse client maze foodLocations mouse
  where notif = "Started mouse #" <> (T.pack (show (identifier mouse)))


application :: WS.ServerApp
application pending = do
    conn <- WS.acceptRequest pending
    eitherConf <- loadConfig conn
    case eitherConf of
        Left err -> Prelude.putStrLn $ "Could not read config: " ++ err
        Right conf -> do
            TIO.putStrLn "Config received!"
            WS.sendTextData conn (T.pack "Config received!")
            let maze = newMaze $ mazeData conf
            let mice = mouseList conf
            let foodListConf = foodList conf
            foodLocations <- makeFoodLocations foodListConf
            TIO.putStrLn "Simulation started!"
            WS.sendTextData conn (T.pack "Simulation started")
            as <- mapM (\m -> async (startMouseService client maze foodLocations m)) mice
            mapM_ wait as
            TIO.putStrLn "Simulation ended!"
          where client = conn
            


        
main :: IO ()
main = WS.runServer "localhost" 4444 $ \pending -> do 
        TIO.putStrLn "Waiting for config from client..."
        application pending
        -- forever $ do
        --     pure()


    -- do
    -- let maze = adjToGraph graphTemp
    -- pos <- newIORef 5
    -- prev <- newIORef 5
    -- forever $ do
    --     p <- readIORef pos
    --     pr <- readIORef prev
    --     (newPos, newPrev) <- mv maze p pr
    --     let mess = "Moving from " ++ (show newPrev) ++ " to " ++ (show newPos)
    --     print mess
    --     writeIORef prev newPrev
    --     writeIORef pos newPos
    --     threadDelay 500000

   


